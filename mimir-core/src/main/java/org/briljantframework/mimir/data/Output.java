/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Isak Karlsson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package org.briljantframework.mimir.data;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.briljantframework.data.series.Convert;

/**
 * An output represents a collection of indexed output variables.
 * 
 * @author Isak Karlsson
 */
public interface Output<T> extends List<T> {

  /**
   * Get the value at the specified position coerced to a value of the given class.
   *
   * @param cls the class
   * @param index the index
   * @param <E> the type of the returned value.
   * @return an instance of the given tyoe
   */
  default <E> E get(Class<? extends E> cls, int index) {
    return Convert.to(cls, index);
  }

  /**
   * Returns the unique labels in this
   * @return
   */
  Set<T> unique();

  Map<T, Integer> counts();
}
